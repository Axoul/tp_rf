/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <stdio.h>
#include <math.h>
#include "Mixer_impl.h"

namespace gr {
  namespace ese {

    Mixer::sptr
    Mixer::make(float gain, float IP3, float R, float NF, float samp_rate)
    {
      return gnuradio::get_initial_sptr
        (new Mixer_impl(gain, IP3, R, NF, samp_rate));
    }

    /*
     * The private constructor
     */
    Mixer_impl::Mixer_impl(float gain, float IP3, float R, float NF, float samp_rate)
      : gr::block("Mixer",
          gr::io_signature::make(1, 1, sizeof(gr_complex)),
          gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      d_gain = gain, d_IP3 = IP3, d_R = R, d_NF = NF, d_samp_rate = samp_rate;
      vip = sqrt(2*d_R*pow(10.0, (d_IP3-30.0)/20));
      k1 = pow(10.0, d_gain/20.0);
      k3 = (-4*k1)/(3*vip*vip);
      pn = 4*1.38*pow(10,-23)*d_R*293*d_samp_rate*(1+pow(10,(d_NF/10)));
      printf("vip = %f\nk1 = %f\nk3 = %f\nPn = %e\n", vip, k1, k3, pn);

    }

    /*
     * Our virtual destructor.
     */
    Mixer_impl::~Mixer_impl()
    {
    }

    void
    Mixer_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    Mixer_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      for(int i = 0; i < noutput_items; i++) {
        //Ajout de bruit gaussien
        gr_complex in_int = (in[i].real()+(pn*pn/2)*gaussian(), in[i].imag()+(pn*pn/2)*gaussian());
        //Conversion Cartésien/Polaire
        float r_in = std::abs(in_int);
        float teta_in = std::arg(in_int);
        //Application sur module
        float r_out = (k1+0.75*k3*r_in*r_in)*r_in;
        //Conversion en cartésien
        out[i] = std::polar(r_out, teta_in);
      }
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    float Mixer_impl::gaussian(void) {
      float va;
      float x1 = 0.1*rand()/(0.1*RAND_MAX);
      float x2 = 0.1*rand()/(0.1*RAND_MAX);
      va = sqrt(-2*log(x1))*cos(2*M_PI*x2);
      return va;
    }

  } /* namespace ese */
} /* namespace gr */
