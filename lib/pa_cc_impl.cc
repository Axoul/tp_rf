/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <stdio.h>
#include <math.h>
#include "pa_cc_impl.h"

namespace gr {
  namespace ese {

    pa_cc::sptr
    pa_cc::make(float gain, float iip3, float R)
    {
      return gnuradio::get_initial_sptr
        (new pa_cc_impl(gain, iip3, R));
    }

    /*
     * The private constructor
     */
    pa_cc_impl::pa_cc_impl(float gain, float iip3, float R)
      : gr::sync_block("PA Amplfier Sync",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      d_gain = gain, d_iip3 = iip3, d_R = R;
    }

    /*
     * Our virtual destructor.
     */
    pa_cc_impl::~pa_cc_impl()
    {
    }

    int
    pa_cc_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      vip = sqrt(2*d_R*pow(10.0, (d_iip3-30.0)/20));
      k1 = pow(10.0, d_gain/20.0);
      k3 = (-4*k1)/(3*vip*vip);

      for(int i = 0; i < noutput_items; i++) {
        //Conversion Cartésien/Polaire
        float r_in = std::abs(in[i]);
        float teta_in = std::arg(in[i]);
        //Application sur module
        float r_out = (k1+0.75*k3*r_in*r_in)*r_in;
        //Conversion en cartésien
        out[i] = std::polar(r_out, teta_in);
      }

      return noutput_items;
    }

    void pa_cc_impl::setup_rpc()
    {
      #ifdef GR_CTRLPORT
      add_rpc_variable(
        rpcbasic_sptr(new rpcbasic_register_get<pa_cc, float>(
                      alias(), "Constant",
                      &pa_cc::gain,
                      pmt::from_double(-4.29e9),
                      pmt::from_double(4.29e9),
                      pmt::from_double(0),
                      "", "Get Gain", RPC_PRIVLVL_MIN,
                      DISPTIME | DISPOPTCPLX | DISPOPTSTRIP)));
      add_rpc_variable(
        rpcbasic_sptr(new rpcbasic_register_set<pa_cc, float>(
                      alias(), "Constant",
                      &pa_cc::set_gain,
                      pmt::from_double(-4.29e9),
                      pmt::from_double(4.29e9),
                      pmt::from_double(0),
                      "", "Set Gain",
                      RPC_PRIVLVL_MIN, DISPNULL)));
      add_rpc_variable(
        rpcbasic_sptr(new rpcbasic_register_get<pa_cc, float>(
                      alias(), "Constant",
                      &pa_cc::iip3,
                      pmt::from_double(-4.29e9),
                      pmt::from_double(4.29e9),
                      pmt::from_double(0),
                      "", "Get IIP3", RPC_PRIVLVL_MIN,
                      DISPTIME | DISPOPTCPLX | DISPOPTSTRIP)));
      add_rpc_variable(
        rpcbasic_sptr(new rpcbasic_register_set<pa_cc, float>(
                      alias(), "Constant",
                      &pa_cc::set_iip3,
                      pmt::from_double(-4.29e9),
                      pmt::from_double(4.29e9),
                      pmt::from_double(0),
                      "", "Set IIP3",
                      RPC_PRIVLVL_MIN, DISPNULL)));
      #endif /* GR_CTRLPORT */
    }

  } /* namespace ese */
} /* namespace gr */
