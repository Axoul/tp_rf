/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_ESE_PA_CC_IMPL_H
#define INCLUDED_ESE_PA_CC_IMPL_H

#include <ese/pa_cc.h>

namespace gr {
  namespace ese {

    class pa_cc_impl : public pa_cc
    {
     private:
       float d_gain, d_iip3, d_R;
       float k1, k3, vip;

     public:
      pa_cc_impl(float gain, float iip3, float R);
      ~pa_cc_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);

     void setup_rpc();
     float gain() const {return d_gain;};
     void set_gain(float gain) {d_gain = gain;};
     float iip3() const {return d_iip3;};
     void set_iip3(float iip3) {d_iip3 = iip3;};
    };


  } // namespace ese
} // namespace gr

#endif /* INCLUDED_ESE_PA_CC_IMPL_H */
