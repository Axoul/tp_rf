/* -*- c++ -*- */
/* 
 * Copyright 2020 gr-ese author.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <vector>
#include <string>
#include <sstream>
#include "iir_ff_impl.h"

namespace gr {
  namespace ese {

    iir_ff::sptr
    iir_ff::make(const char * b_in, const char * a_in)
    {
      return gnuradio::get_initial_sptr
        (new iir_ff_impl(b_in, a_in));
    }

    /*
     * The private constructor
     */
    iir_ff_impl::iir_ff_impl(const char * b_in, const char * a_in)
      : gr::sync_block("iir_ff",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float)))
    {
      std::string b_temp(b_in);
      std::string a_temp(a_in);
      std::stringstream ssb(b_temp);
      std::stringstream ssa(a_temp);
      for (float i; ssb >> i;) {
        b_tabs.push_back(i);
        if (ssb.peek() == ',' || ssb.peek() == ' ')
            ssb.ignore();
      }
      for (float j; ssa >> j;) {
        a_tabs.push_back(j);
        if (ssa.peek() == ',' || ssa.peek() == ' ')
            ssa.ignore();
      }

      in_data.reserve(b_tabs.size());
      for(int k=0; k<b_tabs.size(); k++) {
        in_data.push_back(0.0);
      }

      out_data.reserve(a_tabs.size());
      for(int l=0; l<a_tabs.size(); l++) {
        out_data.push_back(0.0);
      }
      
    }

    /*
     * Our virtual destructor.
     */
    iir_ff_impl::~iir_ff_impl()
    {
    }

    void
    iir_ff_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    iir_ff_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      float *out = (float *) output_items[0];

      // Do <+signal processing+>

      for(int i = 0; i < noutput_items; i++) {

        in_data.insert(in_data.begin(), in[i]);
        in_data.pop_back();

        out_data.insert(out_data.begin(), 0.0);
        out_data.pop_back();

        for(int j = 0; j < b_tabs.size(); j++) {
          out_data[0] += in_data[j]*b_tabs[j];
        }
        
        for(int k = 1; k < a_tabs.size(); k++) {
          out_data[0] -= out_data[k]*a_tabs[k];
        }

        out[i] = out_data[0];
      }
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace ese */
} /* namespace gr */

