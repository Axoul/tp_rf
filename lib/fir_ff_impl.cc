/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <vector>
#include <string>
#include <sstream>
#include "fir_ff_impl.h"

namespace gr {
  namespace ese {

    fir_ff::sptr
    fir_ff::make(const char *tabs_in)
    {
      return gnuradio::get_initial_sptr
        (new fir_ff_impl(tabs_in));
    }

    /*
     * The private constructor
     */
    fir_ff_impl::fir_ff_impl(const char *tabs_in)
      : gr::sync_block("fir_ff",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float)))
    {
      std::string tabs_temp(tabs_in);
      std::stringstream ss(tabs_temp);
      for (float i; ss >> i;) {
        tabs.push_back(i);
        if (ss.peek() == ',' || ss.peek() == ' ')
            ss.ignore();
      }
      in_data.reserve(tabs.size());
      for(int j=0; j<tabs.size(); j++) {
        in_data.push_back(0.0);
      }
    }

    /*
     * Our virtual destructor.
     */
    fir_ff_impl::~fir_ff_impl()
    {
    }

    void
    fir_ff_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    fir_ff_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      float *out = (float *) output_items[0];

      // Do <+signal processing+>

      float out_temp;

      for(int i = 0; i < noutput_items; i++) {

        in_data.insert(in_data.begin(), in[i]);
        in_data.pop_back();

        out_temp = 0.0;
        for(int j = 0; j < tabs.size(); j++) {
          out_temp += in_data[j]*tabs[j];
        }
        out[i] = out_temp;
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace ese */
} /* namespace gr */
