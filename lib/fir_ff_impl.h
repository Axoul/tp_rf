/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_ESE_FIR_FF_IMPL_H
#define INCLUDED_ESE_FIR_FF_IMPL_H

#include <ese/fir_ff.h>
#include <vector>

namespace gr {
  namespace ese {

    class fir_ff_impl : public fir_ff
    {
     private:
       std::vector<float> tabs;
       std::vector<float> in_data;
      // Nothing to declare in this block.

     public:
      fir_ff_impl(const char *tabs_in);
      ~fir_ff_impl();

      void forecast (int noutput_items, gr_vector_int &ninput_items_required);
      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace ese
} // namespace gr

#endif /* INCLUDED_ESE_FIR_FF_IMPL_H */
