/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <stdio.h>
#include <math.h>
#include "PA_Amplifier_impl.h"

namespace gr {
  namespace ese {

    PA_Amplifier::sptr
    PA_Amplifier::make(float gain, float IP3, float R)
    {
      return gnuradio::get_initial_sptr
        (new PA_Amplifier_impl(gain, IP3, R));
    }

    /*
     * The private constructor
     */
    PA_Amplifier_impl::PA_Amplifier_impl(float gain, float IP3, float R)
      : gr::block("PA_Amplifier",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      d_gain = gain, d_IP3 = IP3, d_R = R;
      vip = sqrt(2*d_R*pow(10.0, (d_IP3-30.0)/20));
      k1 = pow(10.0, d_gain/20.0);
      k3 = (-4*k1)/(3*vip*vip);
      printf("vip = %f\nk1 = %f\nk3 = %f\n", vip, k1, k3);
    }

    /*
     * Our virtual destructor.
     */
    PA_Amplifier_impl::~PA_Amplifier_impl()
    {
    }

    void
    PA_Amplifier_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      ninput_items_required[0] = noutput_items;
    }

    int
    PA_Amplifier_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];

      for(int i = 0; i < noutput_items; i++) {
        //Conversion Cartésien/Polaire
        float r_in = std::abs(in[i]);
        float teta_in = std::arg(in[i]);
        //Application sur module
        float r_out = (k1+0.75*k3*r_in*r_in)*r_in;
        //Conversion en cartésien
        out[i] = std::polar(r_out, teta_in);
      }

      // Do <+signal processing+>
      // Tell runtime system how many input items we consumed on
      // each input stream.
      consume_each (noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace ese */
} /* namespace gr */
