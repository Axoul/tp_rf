/* -*- c++ -*- */
/*
 * Copyright 2020 gr-ese author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_ESE_PA_CC_H
#define INCLUDED_ESE_PA_CC_H

#include <ese/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace ese {

    /*!
     * \brief <+description of block+>
     * \ingroup ese
     *
     */
    class ESE_API pa_cc : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<pa_cc> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of ese::pa_cc.
       *
       * To avoid accidental use of raw pointers, ese::pa_cc's
       * constructor is in a private implementation
       * class. ese::pa_cc::make is the public interface for
       * creating new instances.
       */
      static sptr make(float gain, float iip3, float R);
      virtual float gain() const = 0;
      virtual void set_gain(float gain) = 0;
      virtual float iip3() const = 0;
      virtual void set_iip3(float iip3) = 0;
    };

  } // namespace ese
} // namespace gr

#endif /* INCLUDED_ESE_PA_CC_H */
