/* -*- c++ -*- */

#define ESE_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "ese_swig_doc.i"

%{
#include "ese/square.h"
#include "ese/PA_Amplifier.h"
#include "ese/LNA_Amplifier.h"
#include "ese/Mixer.h"
#include "ese/pa_cc.h"
#include "ese/fir_ff.h"
#include "ese/iir_ff.h"
%}


%include "ese/square.h"
GR_SWIG_BLOCK_MAGIC2(ese, square);
%include "ese/PA_Amplifier.h"
GR_SWIG_BLOCK_MAGIC2(ese, PA_Amplifier);
%include "ese/LNA_Amplifier.h"
GR_SWIG_BLOCK_MAGIC2(ese, LNA_Amplifier);
%include "ese/Mixer.h"
GR_SWIG_BLOCK_MAGIC2(ese, Mixer);
%include "ese/pa_cc.h"
GR_SWIG_BLOCK_MAGIC2(ese, pa_cc);
%include "ese/fir_ff.h"
GR_SWIG_BLOCK_MAGIC2(ese, fir_ff);
%include "ese/iir_ff.h"
GR_SWIG_BLOCK_MAGIC2(ese, iir_ff);
