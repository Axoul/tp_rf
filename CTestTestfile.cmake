# CMake generated Testfile for 
# Source directory: /home/axoul/TP_RF/GNU_Radio/OOT/gr-ese
# Build directory: /home/axoul/TP_RF/GNU_Radio/OOT/gr-ese
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include/ese")
subdirs("lib")
subdirs("swig")
subdirs("python")
subdirs("grc")
subdirs("apps")
subdirs("docs")
